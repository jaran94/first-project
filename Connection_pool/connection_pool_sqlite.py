import sqlite3

FLAG_FREE = 0
FLAG_OCCUPIED = 1
FLAG_CORRUPTED = 2

class ConnectionPool:
    def __init__(self, db_path="./sqlite/clientserver.db"):
        self.db_path = db_path
        self.connections_list = []
        self.create_starting_pool(10)


    def create_starting_pool(self, conn_number):
        for x in range(conn_number):
            conn = sqlite3.connect(self.db_path)
            self.connections_list.append([conn, FLAG_FREE])
        print(self.connections_list)

    def add_connections(self):
        if len(self.connections_list) <= 90:
            conn = sqlite3.connect(self.db_path)
            self.connections_list.append([conn, FLAG_FREE])
        else:
            print("Connections number limit has been reached.")

    def get_connection(self):
        for i, connection in enumerate(self.connections_list):
            if connection[1] == FLAG_FREE:
                # set the occupied flag
                self.connections_list[i][1] = FLAG_OCCUPIED
                return i, self.connections_list[i][0]
            elif connection[1] == FLAG_CORRUPTED:
                self.connections_list[i][0].close()
                self.connections_list.pop(i)
                if len(self.connections_list) < 10:
                    self.add_connections()
                print("Connection is corrupted")
        # These two lines will create connection if there isn't any free connection and call again
        print("ADDING NEW CONNECTION AND GIVING AGAIN")
        print(f"Connections before adding: {len(self.connections_list)}")
        self.add_connections()
        print(f"Connections after adding: {len(self.connections_list)}")
        return self.get_connection()

    def return_connection(self, connection_id: int, corrupted=False):
        if not corrupted:
            self.connections_list[connection_id][1] = FLAG_FREE
        else:
            self.connections_list[connection_id][1] = FLAG_CORRUPTED
    def clear_inactive_connections(self):
        for i, connection in enumerate(self.connections_list[10:]):
            if connection[1] == FLAG_FREE or connection[1] == FLAG_CORRUPTED:
                self.connections_list[i][0].close()
                self.connections_list.pop(i)
        print(f"Connection pool cleared. Number of connections: {len(self.connections_list)}")