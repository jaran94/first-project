import json


def create_database(database="users.json"):
	try:
		db = open(database, "r")
		db.close()
		print("Database already created")
	except OSError:
		with open(database, "w") as db:
			print("Database has been created")
			json.dump({"users": []}, db)


def look_for_user(username, database="users.json"):
	with open(database, "r") as db:
		json_db = json.load(db)
		for user in json_db.get("users"):
			if username in user.get("name"):
				return True
		return False


def check_credentials(username, password, database="users.json"):
	with open(database, "r") as db:
		json_db = json.load(db)
		for user in json_db.get("users"):
			if username in user.get("name", "Not found"):
				if user["password"] == password:
					return True
		return False


def add_admin(password, database="users.json"):
	with open(database, "r") as db:
		json_db = json.load(db)
		if look_for_user("admin", database=database):
			return False
		else:
			json_db["users"].append({"name": "admin", "password": password, "inbox": []})
	with open(database, "w") as db:
		json.dump(json_db, db)
	return True


def add_user(username, password, database="users.json"):
	with open(database, "r") as db:
		json_db = json.load(db)
		if look_for_user(username, database=database):
			return False
		else:
			json_db["users"].append({"name": username, "password": password, "inbox": []})
	with open(database, "w") as db:
		json.dump(json_db, db)
	return True


def modify_user(username, new_username="", new_password="", database="users.json"):
	with open(database, "r") as db:
		match = False
		json_db = json.load(db)
		for user in json_db.get("users"):
			if username in user.get("name"):
				if len(new_username) > 0:
					user["name"] = new_username
					match = True
				if len(new_password) > 0:
					user["password"] = new_password
					match = True
	with open(database, "w") as db:
		json.dump(json_db, db)
	return match


def remove_user(username, database="users.json"):
	with open(database, "r") as db:
		match = False
		json_db = json.load(db)
		for user in json_db.get("users"):
			if username in user.get("name"):
				json_db["users"].remove(user)
				match = True
	with open(database, "w") as db:
		json.dump(json_db, db)
	return match


def remove_messages(username, database="users.json"):
	with open(database, "r") as db:
		match = False
		json_db = json.load(db)
		for user in json_db.get("users"):
			if username in user.get("name"):
				user["inbox"].clear()
				match = True
	with open(database, "w") as db:
		json.dump(json_db, db)
	return match


def read_messages(username, database="users.json"):
	with open(database, "r") as db:
		match = False
		json_db = json.load(db)
		for user in json_db.get("users"):
			if username in user.get("name"):
				messages = user["inbox"]
				if not messages:
					return "No new messages."
				match = True
	with open(database, "w") as db:
		json.dump(json_db, db)
	if match:
		return messages
	else:
		return match


def send_messages(sender, recipient, message, database="users.json"):
	with open(database, "r") as db:
		json_db = json.load(db)
		if look_for_user(sender, database=database) and look_for_user(recipient, database=database):
			for user in json_db.get("users"):
				if recipient in user.get("name"):
					if len(user["inbox"]) >= 5:
						return "full"
					else:
						user["inbox"].append(f"{message} | sent by {sender}")
		else:
			return False
	with open(database, "w") as db:
		json.dump(json_db, db)
	return "sent"
