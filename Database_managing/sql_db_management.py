from psycopg import Error
import threading
from Connection_pool.connection_pool import ConnectionPool


class Psql:
	def __init__(self, dbname="clientserverdb", user="postgres"):
		self.timer = None
		self.connection_pool = ConnectionPool(dbname=dbname, user=user)

	def repeat_clear(self):
		self.timer = threading.Timer(interval=60, function=self.repeat_clear)
		self.timer.start()
		self.connection_pool.clear_inactive_connections()

	def cancel_timer(self):
		self.timer.cancel()

	def execution(self, query, parameters=None):
		q = query
		p = parameters
		connection_pack = self.connection_pool.get_connection()
		print(connection_pack)
		connection = connection_pack[1]
		connection_id = connection_pack[0]

		try:
			cur = connection.execute(query, parameters)
			result = cur.fetchall()
			self.connection_pool.return_connection(connection_id)
			return result
		except Error as e:
			self.connection_pool.return_connection(connection_id, corrupted=True)
			print(f"Error raised: {e}")

		self.execution(q, p)

	def look_for_user(self, username):
		query = """SELECT username from users where username = %s"""
		parameters = (username,)
		search = self.execution(query, parameters)
		print("Search result:")
		print(search)
		if username in str(search):
			return True
		else:
			return False

	def check_credentials(self, username, password):
		query = """SELECT * from users where username = %s"""
		parameters = (username,)
		search = self.execution(query, parameters)
		print("Search result:")
		print(search)
		try:
			fetch_username = search[0][0]
			fetch_password = search[0][1]
		except Exception:
			return False
		if username == fetch_username and password == fetch_password:
			return True
		else:
			return False

	def add_admin(self, password):
		if self.look_for_user("admin"):
			return False
		else:
			query = """
				INSERT INTO users (username, password)
				VALUES (%s, %s) RETURNING username
				"""
			parameters = ("admin", password)
			insert = self.execution(query, parameters)
			try:
				if insert[0][0] == "admin":
					return True
				else:
					return False
			except (IndexError, SyntaxError):
				return False

	def add_user(self, username, password):
		if self.look_for_user(username):
			return False
		else:
			query = """
				INSERT INTO users (username, password)
				VALUES (%s, %s) RETURNING username
				"""
			parameters = (username, password)
			insert = self.execution(query, parameters)
			try:
				if insert[0][0] == username:
					return True
				else:
					return False
			except (SyntaxError, IndexError):
				return False

	def modify_user(self, username, new_username="", new_password=""):
		result = False
		if self.look_for_user(username):
			try:
				if len(new_password) > 0:
					query = """UPDATE users SET password = %s WHERE username = %s RETURNING username"""
					parameters = (new_password, username)
					update = self.execution(query, parameters)
					if update[0][0]:
						print(update)
						result = True
				if len(new_username) > 0:
					query = """
					WITH upd AS (
						UPDATE users SET username = %(new_username)s WHERE username = %(username)s RETURNING username
						)
					update messages set username = (select username from upd) WHERE username = %(username)s"""
					parameters = {"new_username": new_username, "username": username}
					update = self.execution(query, parameters)
					if update:
						result = True

			except (SyntaxError, IndexError):
				return False

		return result

	def remove_user(self, username):
		try:
			query = """
			WITH del AS (
				DELETE FROM users WHERE username = %s RETURNING username
				)
			DELETE FROM messages WHERE username = (SELECT username FROM del) RETURNING username;
			"""
			parameters = (username,)
			delete = self.execution(query, parameters)
			if delete:
				print(delete)
				return True
			else:
				return False
		except (SyntaxError, IndexError):
			return False

	def remove_messages(self, username):
		try:
			query = """UPDATE messages SET inbox = NULL where username = %s"""
			parameters = (username,)
			rem_message = self.execution(query, parameters)
			if rem_message:
				print(rem_message)
				return True
			else:
				return False
		except (SyntaxError, IndexError):
			return False

	def read_messages(self, username):
		try:
			query = """SELECT message_id, inbox FROM messages WHERE username = %s"""
			parameters = (username, )
			read_msg = self.execution(query, parameters)
			if type(read_msg) == list:
				messages = ""
				for message in read_msg:
					messages += f"Message_id: {message[0]} Message: {message[1]}\n"
				return messages
			else:
				return "No new messages"
		except (SyntaxError, IndexError):
			return False

	def send_messages(self, sender, recipient, message):
		# Check the number of recipient messages
		try:
			check_inbox_query = """
			SELECT COUNT(inbox) FROM messages
			WHERE username = %s
			"""
			check_inbox_params = (recipient, )
			check_inbox = self.execution(check_inbox_query, check_inbox_params)
			if check_inbox[0][0] < 5:
				msg = f"{message} | sent by {sender}"
				send_query = """
				INSERT INTO messages (username, inbox)
				VALUES (%s, %s) returning inbox
				"""
				send_params = (recipient, msg)
				self.execution(send_query, send_params)
				return "sent"
			else:
				return "full"
		except (SyntaxError, IndexError):
			return False

	def users_list(self):
		try:
			user_query = """
			SELECT username FROM users
			"""
			users_list = self.execution(user_query)
			print(users_list)
			return users_list
		except (SyntaxError, IndexError):
			return False