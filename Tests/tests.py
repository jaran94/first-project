import unittest
import server
import json
from Database_managing import users_database_management as ud
import shutil
import os

class TestServerSimple(unittest.TestCase):
    def test_message_decode(self):
        result = server.message_decode("""{"users": [{"name": "admin", "password": "1111", "inbox": []}]}""".encode("utf-8"))
        target = {"users": [{"name": "admin", "password": "1111", "inbox": []}]}
        self.assertEqual(result, target)

    def test_message_encode(self):
        result = server.encode_message({"users": [{"name": "admin", "password": "1111", "inbox": []}]})
        target = """{"users": [{"name": "admin", "password": "1111", "inbox": []}]}""".encode("utf-8")
        self.assertEqual(result, target)


class TestAddAdmin(unittest.TestCase):
    def test_admin_created(self):
        command = {"command": "add_admin", "password": "1111"}
        result = server.process(command, database="fixtures/users_w_admin.json")
        target = "Admin account has been created already."
        self.assertEqual(result, target, "OK")

    def test_admin_not_created(self):
        command = {"command": "add_admin", "password": "1111"}
        result = server.process(command, database="fixtures/users_wo_admin.json")
        target = "Admin account has been created, please login."
        self.assertEqual(result, target, "OK")
        with open("fixtures/users_wo_admin.json", "r") as f:
            db = str(json.load(f))
            member = "admin"
            self.assertIn(member, db, "OK")
        ud.remove_user("admin", database="fixtures/users_wo_admin.json")


class TestLogin(unittest.TestCase):
    def test_login_admin(self):
        command = {"command": "login", "username": "admin", "password": "1111"}
        result = server.process(command, database="fixtures/users_w_admin.json")
        self.assertEqual(result, server.admin_options())
        server.process({"command": "logout"})

    def test_login_user(self):
        command = {"command": "login", "username": "maurycy", "password": "1234"}
        result = server.process(command, database="fixtures/users_w_admin.json")
        self.assertEqual(result, "maurycy welcome to the server!")
        server.process({"command": "logout"})

    def test_wrong_login(self):
        command = {"command": "login", "username": "xxxx", "password": "xxxx"}
        result = server.process(command, database="fixtures/users_w_admin.json")
        self.assertEqual(result, "Login or password doesn't match, try again.")
        server.process({"command": "logout"})


class TestCreateUser(unittest.TestCase):
    def tearDown(self) -> None:
        ud.remove_user("test_user", database="fixtures/users_w_admin.json")

    def test_create_user(self):
        command = {"command": "create_user", "username": "test_user", "password": "test_password"}
        result = server.process(command, database="fixtures/users_w_admin.json")
        self.assertEqual(result, "test_user has been created, you can now login.")
        with open("fixtures/users_w_admin.json", "r") as f:
            db = str(json.load(f))
            member = "test_user"
            self.assertIn(member, db)

    def test_user_created(self):
        command = {"command": "create_user", "username": "maurycy", "password": "1234"}
        result = server.process(command, database="fixtures/users_w_admin.json")
        self.assertEqual(result, "maurycy is already taken, try something else.")


class TestModifyUser(unittest.TestCase):
    def setUp(self) -> None:
        src = "fixtures/users_wo_admin.json"
        self.dst = "fixtures/modify_user_test.json"
        shutil.copy(src, self.dst)
        server.USER_LOGGED_IN = "maurycy"

    def tearDown(self) -> None:
        os.remove(self.dst)
        server.USER_LOGGED_IN = False
        server.ADMIN_LOGGED_IN = False

    def test_username_true(self):
        command = {"command": "modify_user", "username": "maurycy", "new_username": "mariusz", "password": ""}
        result = server.process(command, database=self.dst)
        target = "Username/password has been changed."
        self.assertEqual(target, result)
        file1 = open("fixtures/users_wo_admin.json", "r")
        file2 = open("fixtures/modify_user_test.json")
        db_before_change = json.load(file1)
        password1 = db_before_change["users"][0]["password"]
        db_after_change = json.load(file2)
        password2 = db_after_change["users"][0]["password"]
        file1.close()
        file2.close()
        self.assertEqual(password1, password2)

    def test_username_no_match(self):
        server.USER_LOGGED_IN = False
        server.ADMIN_LOGGED_IN = True
        command = {"command": "modify_user", "username": "test_user", "new_username": "mariusz", "password": ""}
        result = server.process(command, database=self.dst)
        target = "Username does not exist or login/password is empty."
        self.assertEqual(target, result)

    def test_username_no_access(self):
        command = {"command": "modify_user", "username": "jaroslaw", "new_username": "mariusz", "password": ""}
        result = server.process(command, database=self.dst)
        target = "You can only change your credentials."
        self.assertEqual(target, result)

class TestMessageFunctions(unittest.TestCase):
    def setUp(self) -> None:
        src = "fixtures/users_wo_admin.json"
        self.dst = "fixtures/messages_test.json"
        shutil.copy(src, self.dst)
        server.USER_LOGGED_IN = "maurycy"

    def tearDown(self) -> None:
        os.remove(self.dst)
        server.USER_LOGGED_IN = False

    def test_read_ok(self):
        command = {"command": "read_messages", "username": "maurycy"}
        with open("fixtures/messages_test.json", "r") as db:
            test_db = json.load(db)
            messages = test_db["users"][0]["inbox"]
        target = f"Your messages: \n{messages}"
        result = server.process(command, database=self.dst)
        self.assertEqual(target, result)

    def test_read_empty_list(self):
        command = {"command": "read_messages", "username": "maurycy"}
        result = server.process(command, database=self.dst)
        with open("fixtures/messages_test.json", "r") as db:
            test_db = json.load(db)
            messages = test_db["users"][0]["inbox"]
            self.assertFalse(messages)

    def test_send_message(self):
        command = {"command": "send_messages", "sender": "maurycy", "receiver": "jaroslaw", "message": "test_message"}
        result = server.process(command, database=self.dst)
        with open("fixtures/messages_test.json", "r") as db:
            test_db = json.load(db)
            messages = test_db["users"][1]["inbox"]
            self.assertIn("test_message | sent by maurycy", messages)

    def test_full_inbox(self):
        command = {"command": "send_messages", "sender": "maurycy", "receiver": "jaroslaw", "message": "test_message"}
        with open("fixtures/messages_test.json", "r") as db:
            test_db = json.load(db)
            test_db["users"][1]["inbox"].extend([1, 2, 3, 4, 5])
        with open("fixtures/messages_test.json", "w") as db:
            json.dump(test_db, db)

        result = server.process(command, database=self.dst)
        target = "Receiver inbox is full, cannot deliver the message."
        self.assertEqual(target, result)


if __name__ == '__main__':
    unittest.main()
