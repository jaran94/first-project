import unittest
from Database_managing import sql_db_management
import logging
import time
import concurrent.futures

ud = sql_db_management.Psql(dbname="test_sql")


class TestConnectionPool(unittest.TestCase):
    def setUp(self) -> None:
        logging.basicConfig(filename="tests_connection_pool.log", encoding="utf-8", level=logging.DEBUG, filemode="w")
        logging.info("Started")
        ud.add_user(username="overload_test", password="1111")
        ud.repeat_clear()

    def tearDown(self) -> None:
        ud.cancel_timer()
        logging.info("Finished")


    def test_connections(self):
        look_args = ['overload_test', "non_existent", "dupa"]

        start = time.time()
        while time.time() - start < 10:
            with concurrent.futures.ThreadPoolExecutor(max_workers=40) as executor:
                new = executor.map(ud.look_for_user, look_args)
                print(list(new))


if __name__ == '__main__':
    unittest.main()
