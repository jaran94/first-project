import unittest
import server
from Database_managing import sql_db_management

ud = sql_db_management.Psql(dbname="test_sql")


class TestAddAdmin(unittest.TestCase):
    def test_admin_created(self):
        ud.add_admin("1234")
        command = {"command": "add_admin", "password": "1111"}
        result = server.process(command, dbname="test_sql")
        target = "Admin account has been created already."
        self.assertEqual(target, result)
        ud.remove_user("admin")

    def test_admin_not_created(self):
        ud.remove_user("admin")
        command = {"command": "add_admin", "password": "1111"}
        result = server.process(command, dbname="test_sql")
        target = "Admin account has been created, please login."
        self.assertEqual(target, result)
        self.assertTrue(ud.look_for_user("admin"))
        ud.remove_user("admin")


class TestLogin(unittest.TestCase):
    def setUp(self) -> None:
        ud.add_admin("1111")
        ud.add_user("test_user", "1234")

    def tearDown(self) -> None:
        ud.execution("""TRUNCATE users, messages""")

    def test_login_admin(self):
        command = {"command": "login", "username": "admin", "password": "1111"}
        result = server.process(command, dbname="test_sql")
        self.assertEqual(server.admin_options(), result)
        server.process({"command": "logout"}, dbname="test_sql")

    def test_login_user(self):
        command = {"command": "login", "username": "test_user", "password": "1234"}
        result = server.process(command, dbname="test_sql")
        self.assertEqual("test_user welcome to the server!", result)
        server.process({"command": "logout"}, dbname="test_sql")

    def test_wrong_login(self):
        command = {"command": "login", "username": "xxxx", "password": "xxxx"}
        result = server.process(command, dbname="test_sql")
        self.assertEqual("Login or password doesn't match, try again.", result)
        server.process({"command": "logout"}, dbname="test_sql")


class TestCreateUser(unittest.TestCase):
    def setUp(self) -> None:
        ud.add_user("test_user", "1234")

    def tearDown(self) -> None:
        ud.remove_user("test_user")

    def test_create_user(self):
        ud.remove_user("test_user")
        command = {"command": "create_user", "username": "test_user", "password": "test_password"}
        result = server.process(command, dbname="test_sql")
        self.assertEqual("test_user has been created, you can now login.", result)
        self.assertTrue(ud.look_for_user("test_user"))

    def test_user_created(self):
        command = {"command": "create_user", "username": "test_user", "password": "1234"}
        result = server.process(command, dbname="test_sql")
        self.assertEqual("test_user is already taken, try something else.", result)


class TestModifyUser(unittest.TestCase):
    def setUp(self) -> None:
        ud.add_user("maurycy", "1234")
        ud.add_user("jaroslaw", "1234")
        server.USER_LOGGED_IN = "maurycy"

    def tearDown(self) -> None:
        ud.execution("""TRUNCATE users, messages""")
        server.USER_LOGGED_IN = False

    def test_username_true(self):
        password1 = ud.execution("""SELECT password from users where username='maurycy'""")
        print(password1)
        command = {"command": "modify_user", "username": "maurycy", "new_username": "mariusz", "password": ""}
        result = server.process(command, dbname="test_sql")
        print(ud.execution("""SELECT * from users"""))
        target = "Username/password has been changed."
        self.assertEqual(target, result)
        password2 = ud.execution("""SELECT password from users where username='mariusz'""")
        self.assertEqual(password1, password2)

    def test_username_no_match(self):
        server.USER_LOGGED_IN = False
        server.ADMIN_LOGGED_IN = True
        command = {"command": "modify_user", "username": "test_user", "new_username": "mariusz", "password": ""}
        result = server.process(command, dbname="test_sql")
        target = "Username does not exist or login/password is empty."
        self.assertEqual(target, result)

    def test_username_no_access(self):
        command = {"command": "modify_user", "username": "jaroslaw", "new_username": "mariusz", "password": ""}
        result = server.process(command, dbname="test_sql")
        target = "You can only change your credentials."
        self.assertEqual(target, result)


class TestMessageFunctions(unittest.TestCase):
    def setUp(self) -> None:
        ud.add_user("maurycy", "1234")
        ud.add_user("jaroslaw", "1234")
        server.USER_LOGGED_IN = "maurycy"

    def tearDown(self) -> None:
        ud.remove_user("maurycy")
        ud.remove_user("jaroslaw")
        server.USER_LOGGED_IN = False

    def test_read_ok(self):
        command = {"command": "read_messages", "username": "maurycy"}
        target = "Your messages: \n" + ud.read_messages("maurycy")
        result = server.process(command, dbname="test_sql")
        self.assertEqual(target, result)

    def test_send_message(self):
        command = {"command": "send_messages", "sender": "maurycy", "receiver": "jaroslaw", "message": "test_message"}
        server.process(command, dbname="test_sql")
        messages = ud.read_messages("jaroslaw")
        self.assertIn("test_message | sent by maurycy", messages)

    def test_full_inbox(self):
        ud.execution("""
                INSERT INTO messages (username, inbox)
                VALUES
                    ('jaroslaw', 'message1'),
                    ('jaroslaw', 'message2'),
                    ('jaroslaw', 'message3'),
                    ('jaroslaw', 'message4'),
                    ('jaroslaw', 'message5')""")
        command = {"command": "send_messages", "sender": "maurycy", "receiver": "jaroslaw", "message": "test_message"}

        result = server.process(command, dbname="test_sql")
        target = "Receiver inbox is full, cannot deliver the message."
        self.assertEqual(target, result)


if __name__ == '__main__':
    unittest.main()
