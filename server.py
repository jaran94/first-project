import socket
import time
import json
from Database_managing import sqlite_db_management
from Database_managing.user import User


start_time = time.time()
HOST = "127.0.0.1"
PORT = 65432
VERSION = "0.7"
CREATION_DATE = "2022-08-07"
USER_LOGGED_IN = False
ADMIN_LOGGED_IN = False
server_up = True


def message_decode(msg_bytes):
	msg_json = msg_bytes.decode("utf-8")
	msg = json.loads(msg_json)
	return msg


def encode_message(msg):
	msg_json = json.dumps(msg)
	msg_bytes = msg_json.encode("utf-8")
	return msg_bytes


def uptime_counter():
	uptime = time.time() - start_time
	msg = f"Uptime of the server: {uptime} s"
	return msg


def server_info():
	msg = f"Version of the server {VERSION}, Date of creation: {CREATION_DATE}"
	return msg


def server_help():
	msg = """
	Hello, here are all the commands you can use:
	uptime: Returns uptime of the server.
	info: Returns version info and creation date of the server.
	help: Returns list of commands with descriptions.
	stop: Stops the server and client simultaneously.
	login: Login with your username and password. 
	logout: Logs you out.
	create_user: Create new user.
	modify_user: Modify your username or password.
	read_messages: Read unread messages.
	send_messages: Send message to user. 
				"""
	return msg


def admin_options():
	# add next options after message implementation
	msg = """
	Hello, here are all admin specific options:
	delete_user: Delete specific user.
	modify_user: Modify specific user.
	users_list: Shows all users. 
				"""
	return msg


def server_stop():
	msg = "You wish is my desire, server closed, status: stopped"
	global server_up
	server_up = False
	return msg


def process(command, database_obj):
	global USER_LOGGED_IN
	global ADMIN_LOGGED_IN
	ud = database_obj
	match command.get("command"):
		case "add_admin":
			user = User("admin", command.get("password"))
			if ud.add_admin(user.password):
				return "Admin account has been created, please login."
			else:
				return "Admin account has been created already."
		case "login":
			user = User(command.get("username"), command.get("password"))
			if USER_LOGGED_IN or ADMIN_LOGGED_IN:
				return "You're already logged in"
			else:
				if ud.check_credentials(user.name, user.password):
					if user.name == "admin":
						ADMIN_LOGGED_IN = True
						print("Admin logged")
						return admin_options()
					else:
						USER_LOGGED_IN = user.name
						print(f"User {user.name} logged")
						return f"{user.name} welcome to the server!"
				else:
					return "Login or password doesn't match, try again."
		case "logout":
			if ADMIN_LOGGED_IN or USER_LOGGED_IN:
				ADMIN_LOGGED_IN = False
				USER_LOGGED_IN = False
				return "You've been logged out."
			else:
				return "You're not logged in, hence you can't logout."
		case "create_user":
			user = User(command.get("username"), command.get("password"))
			if ud.add_user(user.name, user.password):
				return f"{user.name} has been created, you can now login."
			else:
				return f"{user.name} is already taken, try something else."
		case "delete_user":
			user = User(command.get("username"))
			if ADMIN_LOGGED_IN:
				if ud.remove_user(user.name):
					return f"User {user.name} has been removed."
				else:
					return "No such username in the database"
			else:
				return "You have no rights for it."
		case "modify_user":
			user = User(command.get("username"), command.get("password"))
			new_username = command.get("new_username")
			if USER_LOGGED_IN == user.name or ADMIN_LOGGED_IN:
				result = ud.modify_user(user.name, new_username, user.password)
				if result:
					USER_LOGGED_IN = new_username
					user.name = new_username
					return "Username/password has been changed."
				else:
					return "Username does not exist or login/password is empty."
			else:
				return "You can only change your credentials."
		case "users_list":
			if ADMIN_LOGGED_IN:
				try:
					users_list = ud.users_list()
					users_string = "\n".join([word[0] for word in users_list])
					return f"Here's all the users: \n{users_string}"
				except (TypeError, ValueError) as e:
					return f"Error has been raised: {e}"
			else:
				return "You have no privileges to see users list"

		case "read_messages":
			user = User(command.get("username"))
			messages = ud.read_messages(user.name)
			if USER_LOGGED_IN == user.name or ADMIN_LOGGED_IN:
				if messages:
					ud.remove_messages(user.name)
					return f"Your messages: \n{messages}"
				else:
					return "Wrong username."
			else:
				return "You're not logged in."
		case "send_messages":
			sender = command.get("sender")
			receiver = command.get("receiver")
			msg = command.get("message")
			if USER_LOGGED_IN == sender or ADMIN_LOGGED_IN:
				message_status = ud.send_messages(sender, receiver, msg)
				if message_status == "sent":
					return "Message has been sent."
				elif message_status == "full":
					return "Receiver inbox is full, cannot deliver the message."
				else:
					return "Message has not been sent: wrong sender or receiver."
			else:
				return "You're not logged in."
		case "info":
			msg = server_info()
			return msg
		case "uptime":
			msg = uptime_counter()
			return msg
		case "help":
			msg = server_help()
			return msg
		case "stop":
			msg = server_stop()
			return msg
		case _:
			return "Nothing matches."


def main():
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.bind((HOST, PORT))
		global server_up
		while server_up:
			s.listen()
			print("Server started, waiting for connection...")
			conn, addr = s.accept()
			with conn:
				# ud = sql_db_management.Psql()
				# new ud for sqlite below
				ud = sqlite_db_management.Sqlite()
				ud.repeat_clear()

				if not ud.look_for_user("admin"):
					message = """Admin user is not created. Please use command "add_admin" to create admin user"""
					print(f"Message sent: {message}")
					encoded_message = encode_message(message)
					conn.sendall(encoded_message)
				else:
					message = server_help()
					print(f"Message sent: {message}")
					encoded_message = encode_message(message)
					conn.sendall(encoded_message)
				print(f"Connected by {addr}")
				while True:
					data = conn.recv(1024)
					if not data:
						print("Connection error")
						server_up = False
						ud.cancel_timer()
						break
					# TODO wrap it in try somehow
					try:
						incoming = message_decode(data)
						print(incoming)
					except ValueError:
						incoming = "JSON decode has failed on server side, no or wrong data has been sent"

					message = process(incoming, ud)
					encoded_message = encode_message(message)
					conn.sendall(encoded_message)


if __name__ == "__main__":
	main()
